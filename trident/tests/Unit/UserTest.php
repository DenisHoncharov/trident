<?php

namespace Tests\Unit;

use App\Product;
use App\User;
use App\WishList;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /** @test */
    public function it_can_have_product()
    {
        $user = factory(User::class)->create();

        $user->products()->create(factory(Product::class)->raw());

        $this->assertInstanceOf(Product::class, $user->products()->first());
    }

    /** @test */
    public function it_can_have_wishlist()
    {
        $user = factory(User::class)->create();

        $user->wishlist()->create(factory(WishList::class)->raw());

        $this->assertInstanceOf(WishList::class, $user->wishlist()->first());
    }

    /** @test */
    public function it_can_create_own_product()
    {
        $user = factory(User::class)->create();

        $user->createProduct(factory(Product::class)->raw());

        $this->assertInstanceOf(Product::class, $user->products()->first());
    }

    /** @test */
    public function it_can_create_own_wishlist()
    {
        $user = factory(User::class)->create();

        $user->createWishList(factory(WishList::class)->raw());

        $this->assertInstanceOf(WishList::class, $user->wishlist()->first());
    }
}
