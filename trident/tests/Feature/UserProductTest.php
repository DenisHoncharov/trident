<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserProductTest extends TestCase
{
    use RefreshDatabase, DatabaseMigrations;

    /** @test */
    public function an_user_can_see_all_products()
    {
        $product = factory(Product::class)->create();

        $this->getJson('/api/products')
            ->assertSee($product->title);
    }

    /** @test */
    public function an_unauthenticated_user_can_not_create_product()
    {
        $this->withExceptionHandling();

        $attributes = [
            'title' => 'Foo'
        ];

        $this->postJson('/api/products', $attributes)
            ->assertStatus(401);

        $this->assertDatabaseMissing('products', $attributes);
    }

    /** @test */
    public function an_authenticated_user_can_create_product()
    {
        $attributes = [
            'title' => 'Foo'
        ];

        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->postJson('/api/products', $attributes)
            ->assertSee($attributes['title']);

        $this->assertDatabaseHas('products', $attributes);
    }

    /** @test */
    public function a_product_must_have_title()
    {
        $user = factory(User::class)->create();

        $attributes = [
            'title' => ''
        ];

        $this->actingAs($user, 'api')
            ->postJson('/api/products', $attributes)
            ->assertJsonValidationErrors('title');
    }

    /** @test */
    public function a_authenticated_user_can_delete_an_own_product()
    {
        $user = factory(User::class)->create();

        $products = factory(Product::class, 2)->create([
            'user_id' => $user->id
        ]);

        $this->actingAs($user, 'api')
            ->deleteJson('/api/products/'. $products[0]->id)
            ->assertRedirect('/api/products');

        $this->assertCount(1, $user->products);
    }

    /** @test */
    public function a_authenticated_user_can_not_delete_foreign_product()
    {
        $user = factory(User::class)->create();

        $products = factory(Product::class, 2)->create();

        $this->actingAs($user, 'api')
            ->deleteJson('/api/products/'. $products[0]->id)
            ->assertStatus(403);

        $this->assertCount(2, Product::all());
    }

    /** @test */
    public function a_unauthenticated_user_can_not_delete_a_product()
    {
        $products = factory(Product::class, 2)->create();

        $this->deleteJson('/api/products/'. $products[0]->id)
            ->assertStatus(401);

        $this->assertCount(2, Product::all());
    }

    /** @test */
    public function a_authenticated_user_can_update_an_own_product()
    {
        $user = factory(User::class)->create();

        $product = factory(Product::class)->create([
            'user_id' => $user->id
        ]);

        $attributes = [
            'title' => 'Foo'
        ];

        $this->actingAs($user, 'api')
            ->patchJson('/api/products/' . $product->id, $attributes)
            ->assertStatus(204);

        $this->assertDatabaseHas('products', $attributes);
    }

    /** @test */
    public function a_authenticated_user_can_not_change_owner_of_an_product()
    {
        $user = factory(User::class)->create();

        $userTwo = factory(User::class)->create();

        $product = factory(Product::class)->create([
            'user_id' => $user->id
        ]);

        $attributes = $expectedAttributes = [
            'title' => 'Foo',
            'user_id' => $userTwo->id
        ];

        $expectedAttributes['user_id'] = $user->id;

        $this->actingAs($user, 'api')
            ->patchJson('/api/products/' . $product->id, $attributes)
            ->assertStatus(204);

        $this->assertDatabaseHas('products', $expectedAttributes);
    }

    /** @test */
    public function a_authenticated_user_can_not_update_a_foreign_product()
    {
        $user = factory(User::class)->create();

        $product = factory(Product::class)->create();

        $attributes = [
            'title' => 'Foo'
        ];

        $this->actingAs($user, 'api')
            ->patchJson('/api/products/' . $product->id, $attributes)
            ->assertStatus(403);

        $this->assertDatabaseMissing('products', $attributes);
    }

    /** @test */
    public function an_unauthenticated_user_can_not_update_a_product()
    {
        $product = factory(Product::class)->create();

        $attributes = [
            'title' => 'Foo'
        ];

        $this->patchJson('/api/products/' . $product->id, $attributes)
            ->assertStatus(401);

        $this->assertDatabaseMissing('products', $attributes);
    }
}
