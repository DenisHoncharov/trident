<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportUsersWishListCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Export user's wish list in CSV";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wishLilts = \App\WishList::query()
            ->withCount('products')
            ->with([
                'creator:id,name'
            ])->get();

        $fp = fopen('wish-list.csv', 'w');

        $wishLilts->map(function ($wishList) use ($fp) {
            fputcsv($fp, [$wishList->creator->name, $wishList->title, $wishList->products_count], ';');
        });

        fclose($fp);
    }
}
