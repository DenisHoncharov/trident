<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $fillable = [
        'user_id',
        'title'
    ];

    protected $with = [
        'creator'
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function addProduct($product)
    {
        return $this->products()->attach($product);
    }

    public function removeProduct($product)
    {
        return $this->products()->detach($product->id);
    }
}
