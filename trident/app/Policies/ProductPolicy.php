<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function update(User $user, Product $product)
    {
        return $this->manage($user, $product);
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function delete(User $user, Product $product)
    {
        return $this->manage($user, $product);
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    private function manage(User $user, Product $product)
    {
        return $user->is($product->user);
    }
}
