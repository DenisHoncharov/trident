<?php

namespace App\Http\Controllers;

use App\Product;
use App\WishList;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class WishListProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param WishList $wishList
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function index(WishList $wishList)
    {
        $this->authorize('view', $wishList);

        return response()->json($wishList->products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WishList $wishList
     * @param Product $product
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(WishList $wishList, Product $product)
    {
        $this->authorize('create', $wishList);

        $wishList->addProduct($product);;

        return response()->json($wishList, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param WishList $wishList
     * @param Product $product
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function destroy(WishList $wishList, Product $product)
    {
        $this->authorize('delete', $wishList);

        $wishList->removeProduct($product);

        return redirect()->home();
    }
}
