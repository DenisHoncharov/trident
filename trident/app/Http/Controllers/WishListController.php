<?php

namespace App\Http\Controllers;

use App\Http\Requests\WishListRequest;
use App\WishList;
use App\User;

class WishListController extends Controller
{

    public function index(User $user)
    {
        $wishlist = $user->wishlist;

        return response()->json($wishlist);
    }

    public function store(WishListRequest $request)
    {
        $wishlist = auth()->user()->createWishList($request->except('user_id'));

        return response()->json($wishlist);
    }

    public function show(WishList $wishList)
    {
        $this->authorize('view', $wishList);

        $wishList = $wishList->toArray();

        return response()->json($wishList);
    }

    public function update(WishList $wishList, WishListRequest $request)
    {
        $this->authorize('update', $wishList);

        $wishList = $wishList->update($request->except('user_id'));

        return response()->json($wishList, 204);
    }

    public function destroy(WishList $wishList)
    {
        $this->authorize('delete', $wishList);

        $wishList->delete();

        return redirect()->home();
    }
}
