<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//show all products list
Route::get('/products', 'ProductsController@index');

Route::middleware('auth:api')->group(function () {

    //show products in user wishlist (work only for user own wishlist)
    Route::get('/wish-list/{wishList}/products/', 'WishListProductController@index');

    //add product to own wishlist
    Route::post('/wish-list/{wishList}/products/{product}', 'WishListProductController@store');

    //remove product from own wishlist
    Route::delete('/wish-list/{wishList}/products/{product}', 'WishListProductController@destroy');

    //create new product
    Route::post('/products', 'ProductsController@store');

    //update own product
    Route::patch('/products/{product}', 'ProductsController@update');

    //destroy own product
    Route::delete('/products/{product}', 'ProductsController@destroy');

    //show own wishlists
    Route::get('/wish-list/{user}', 'WishListController@index');

    //show own wishlist info
    Route::get('/wish-list/show/{wishList}', 'WishListController@show');

    //create own wishlist
    Route::post('/wish-list', 'WishListController@store');

    //update own wishlist
    Route::patch('/wish-list/{wishList}', 'WishListController@update');

    //delete own wish list
    Route::delete('/wish-list/{wishList}', 'WishListController@destroy');
});
