# Trident

Instruction to start development:

Clone this git repository<br>
Enter to main folder<br>
Execute follow commands:
  
    $ chmod 0777 -R ./databases
    $ docker-compose up --build -d
    $ docker exec -ti api-web composer install
    
    $ docker exec -ti api-web php artisan migrate
    $ docker exec -ti api-web php artisan db:seed
    
    Open hosts file in texteditor:
    $ sudo nano /etc/hosts
    
    Insert to hosts file next row:
    127.0.0.1    trident.local
    
    Open http://trident.local:8080/ in your browser
    
To make database dump:
    
    $ docker exec db /usr/bin/mysqldump -u root --password=toor moodle > ./databases/trident.sql
    
Restore database from backup:
    
    $ cat ./databases/trident.sql | docker exec -i db /usr/bin/mysql -u root --password=toor trident

***
  
To run tests you can execute (in 'trident' folder):

    $ vendor/bin/phpunit
    
or (in root folder):

    $ docker exec -ti api-web vendor/bin/phpunit

***

To retrieve csv data export execute:

    $ docker exec -ti api-web php artisan export:csv
  
    